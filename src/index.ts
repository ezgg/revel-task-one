const dictionary: any = {
    singles: {
        0: 'zero',
        1: 'one',
        2: 'two',
        3: 'three',
        4: 'four',
        5: 'five',
        6: 'six',
        7: 'seven',
        8: 'eight',
        9: 'nine',
    },
    tens: {
        10: 'ten',
        11: 'eleven',
        12: 'twelve',
        13: 'thirteen',
        14: 'fourteen',
        15: 'fifteen',
        16: 'sixteen',
        17: 'seventeen',
        18: 'eighteen',
        19: 'nineteen'
    },
    decimals: {
        1: 'ten',
        2: 'twenty',
        3: 'thirty', 
        4: 'forty',
        5: 'fifty',
        6: 'sixty',
        7: 'seventy',
        8: 'eighty',
        9: 'ninety'
    }
};
export function convertNumberToEnglishText(n: number): string {
    if (n === 0) {
      return "zero";
    }
  
    const absolute = Math.abs(n);
    const tenKilo = Math.floor(absolute / 10000);
    const kilo = Math.floor((absolute - tenKilo * 10000) / 1000);
    const cent = Math.floor((absolute - kilo * 1000 - tenKilo * 10000) / 100);
    const dec = Math.floor(
      (absolute - tenKilo * 10000 - kilo * 1000 - cent * 100) / 10
    );
    const single = Math.floor(
      absolute - tenKilo * 10000 - kilo * 1000 - cent * 100 - dec * 10
    );
    const printable = [];
  
    if (n < 0) {
      printable.push("negative");
    }
  
    if (tenKilo && kilo) {
      if (tenKilo > 1) {
        printable.push(
          dictionary.decimals[`${tenKilo}`],
          dictionary.singles[`${kilo}`],
          "thousand"
        );
      } else {
        printable.push(dictionary.tens[`${Math.floor(absolute / 1000)}`], "thousand");
      }
    }
  
    if (tenKilo && !kilo) {
      printable.push(dictionary.decimals[`${tenKilo}`], "thousand");
    }
  
    if (!tenKilo && kilo) {
      printable.push(dictionary.singles[`${kilo}`], "thousand");
    }
  
    if (cent) {
      printable.push(dictionary.singles[`${cent}`], "hundred");
    }
  
    if (dec && single) {
      if (dec > 1) {
        printable.push(
          dictionary.decimals[`${dec}`],
          dictionary.singles[`${single}`]
        );
      } else {
        printable.push(
          dictionary.tens[`${absolute - (tenKilo * 10000 + kilo * 1000 + cent * 100)}`]
        );
      }
    }
  
    if (dec && !single) {
      printable.push(dictionary.decimals[`${dec}`]);
    }
  
    if (!dec && single) {
      printable.push(dictionary.singles[`${single}`]);
    }
  
    return printable.join(" ");
}
